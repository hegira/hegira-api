/**
 * Copyright 2015 Marco Scavuzzo
 * Contact: Marco Scavuzzo <marco.scavuzzo@polimi.it>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 
 */
package it.polimi.hegira.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Marco Scavuzzo
 *
 */
public class PropertiesManager {
	private static Logger log = LoggerFactory.getLogger(PropertiesManager.class);
	private static String defaultPath = System.getProperty("user.home")+File.separator+
			"hegira"+File.separator+
			"api"+File.separator;
	
	/**
	 * Gets the value of a given property stored inside the given file.
	 * @param fileName the file name in the default path (user.home)
	 * @param property	The name of the property to retrieve.
	 * @return	The value associated to the given property name.
	 */
	public static String getProperty(String fileName, String property){
		Properties props = new Properties();
		InputStream isr = null;
		try {
			isr = new FileInputStream(createFileIfNotExists(defaultPath, fileName));	
			props.load(isr);
			return props.getProperty(property);
		} catch (IOException e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					fileName != null ? defaultPath+fileName : "thath should be located in "+defaultPath,
					e);
		} catch (Exception e) {
			log.error("{} - Error reading file {}.\nStack Trace:",
					Thread.currentThread().getName(),
					fileName != null ? defaultPath+fileName : "thath should be located in "+defaultPath,
					e);
		} finally {
			try {
				if(isr!=null)
					isr.close();
			} catch (IOException e) {}
			props=null;
		}
		return null;
	}
	
	
	private static String getPropertyFromFileInJar(String file, String propertyKey){
		
		Properties props = new Properties();
		URL resource = Thread.currentThread().getContextClassLoader().getResource(file);
		
		try {
			InputStream isr = new FileInputStream(resource.getFile());
			props.load(isr);
			return props.getProperty(propertyKey);
		} catch (FileNotFoundException | NullPointerException e) {
			log.error(file+" file must exist!");
		} catch (IOException e) {
			log.error("Unable to read file "+file+"!");
		} finally {
			props=null;
		}
		
		return null;
	}
	
	public static String getQueueProperty(String propertyKey){
		return getProperty(Constants.QUEUE_PATH, propertyKey);
	}
	
	public static String getZkProperty(String propertyKey){
		return getProperty(Constants.ZK_PATH, propertyKey);
	}
	
	private static File createFileIfNotExists(String path, String fileName) 
			throws NullPointerException, IOException{
		if(fileName == null)
			throw new NullPointerException("fileName cannot be null");
		if(path==null)
			path=defaultPath;
		
		File file = new File(path+fileName);
		if (!file.getParentFile().exists())
		    file.getParentFile().mkdirs();
		if (!file.exists())
		    file.createNewFile();
		return file;
	}
}
